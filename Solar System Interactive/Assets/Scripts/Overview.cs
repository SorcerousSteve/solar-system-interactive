﻿using UnityEngine;
using System.Collections;

public class Overview : MonoBehaviour {

	public GameObject objectToMoveOverview;
	
	// Update is called once per frame
	void OnClick () {
		
		objectToMoveOverview.animation.Play("Overview");
		
	}
}
