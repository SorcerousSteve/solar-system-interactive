﻿using UnityEngine;
using System.Collections;

public class HideInstructions : MonoBehaviour {

	public GameObject objectToMoveInstruction;
	
	// Update is called once per frame
	void OnClick () {
		
		objectToMoveInstruction.animation.Play("HideInstructions");
		
	}
}

