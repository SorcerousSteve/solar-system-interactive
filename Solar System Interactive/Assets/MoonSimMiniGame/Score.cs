﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {
	public GUIText scoreText;
	public int score;
	bool carrying;
	public GameObject gameController;

	// Use this for initialization
	void Start () {
		score = 0;
		UpdateScore();
	
	}


	public void AddScore (int newScoreValue) {
		score += newScoreValue;
		UpdateScore();
	}

	// Update is called once per frame
	void UpdateScore () {

			scoreText.text = "Score: " + score;


	}
}
