﻿using UnityEngine;
using System.Collections;

public class Pickupable : MonoBehaviour {
	public GUIText scoreText;
	public int score;
	public int scoreValue;

	// Use this for initialization
	void Start () {

	}


	public void AddScore (int newScoreValue) {
		score += newScoreValue;
		UpdateScore();
	}
	
	// Update is called once per frame
	void UpdateScore () {
		
		scoreText.text = "Score: " + score;
		
	}
}
